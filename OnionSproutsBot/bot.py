# SPDX-FileCopyrightText: 2020-2023 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2022 Evangelos "GeopJr" Paterakis
# SPDX-FileCopyrightText: 2022 Justin "Justasic" Crawford
# SPDX-FileCopyrightText: 2022-2023 The Tor Project, Inc.
# SPDX-FileContributor: 2022-2023 irl <irl@sr2.uk>
#
# SPDX-License-Identifier: BSD-3-Clause

import asyncio
import logging
from datetime import datetime as dt

from pyrogram import Client, idle

from . import i18n
from .config import data
from .database import Database

plugins = dict(root="OnionSproutsBot.plugins")

Bot = Client(
    "OnionSproutsBot",
    data["telegram"]["api_id"],
    data["telegram"]["api_hash"],
    bot_token=data["telegram"]["bot_token"],
    in_memory=True,
    plugins=plugins,
)

i18n.setup_gettext()


async def main():
    """
    We take care of all of the logging stuff before we
    do anything else. There definitely has to be some sort
    of a filename though, so we hardcode "OSB" in case
    nothing else is available.
    """

    # We start off with the path where files will be stored.
    # If one hasn't been defined, it will be left blank and
    # the default directory will be the current working one.
    log_filename = str(data["logging"]["directory"] or "")

    # Carrying on with the file name...
    if data["logging"]["filename_suffix"] is not None:
        log_filename += data["logging"]["filename_suffix"]
    else:
        log_filename += "OSB"

    # We concatenate the date and the time for better organization...
    # As always, this is optional, but configured by default.
    if data["logging"]["filename_datefmt"] is not None:
        # Adds separator between name and date, if defined.
        log_filename += str(data["logging"]["date_separator"] or "")
        # Adds date depending on format defined in the configuration.
        log_filename += dt.now().strftime(data["logging"]["filename_datefmt"])

    logging.basicConfig(
        filename=f'{log_filename}.log',
        format=data['logging']['format'],
        encoding=data['logging']['encoding'],
        level=logging.INFO,
        datefmt=f"{data['logging']['datefmt']}"
    )
    logging.getLogger().addHandler(logging.StreamHandler())  # prints to stderr

    # Okay, here comes the real deal.
    logging.info("=== OnionSproutsBot ===")

    # Custom paths
    if data["bot"]["download_path"] is not None:
        logging.info(f"Custom download path: {data['bot']['download_path']}")

    await Database().connect(data['bot']['db_name'], data['bot']['db_path'])
    await Database().create_empty_db()

    logging.info("Connection to database initiated.")

    # Call Pyrogram's idle forever function
    await Bot.start()
    await idle()
    await Database().close()


loop = asyncio.get_event_loop()


def init():
    loop.run_until_complete(main())
