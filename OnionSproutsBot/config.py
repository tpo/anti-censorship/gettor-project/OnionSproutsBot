# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
# SPDX-FileContributor: 2022-2023 irl <irl@sr2.uk>
#
# SPDX-License-Identifier: BSD-3-Clause

import sys

import yaml

with open(sys.argv[1], "r") as config:
    data = yaml.safe_load(config)
