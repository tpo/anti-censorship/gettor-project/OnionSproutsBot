# SPDX-FileCopyrightText: 2022-2024 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2022 Evangelos "GeopJr" Paterakis
# SPDX-FileCopyrightText: 2022 Alain Zscheile
# SPDX-FileCopyrightText: 2022 WofWca
# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
# SPDX-FileContributor: 2023 irl <irl@sr2.uk>
#
# SPDX-License-Identifier: BSD-3-Clause

from typing import Optional, Sequence

import aiohttp
from pyrogram import Client
from pyrogram.types import CallbackQuery, InlineKeyboardButton, InlineKeyboardMarkup

from .i18n import get_translation


# Helper function for updating the list of available binaries
async def get_response(endpoint):
    async with aiohttp.ClientSession() as session:
        async with session.get(endpoint) as resp:
            return await resp.json()


def unpack_callback_data(callback: CallbackQuery) -> Sequence[str]:
    data = callback.data
    if data is None:
        return []
    if isinstance(data, bytes):
        data = data.decode("utf-8")
    return data.split(":")


async def sig_send_notification(
        client: Client, callback: CallbackQuery, user_lang: str
):
    _ = get_translation(user_lang)

    return await client.send_message(
        callback.from_user.id, _("We are uploading the signature file first...")
    )


async def file_send_notification(
        client: Client, callback: CallbackQuery, user_lang: str
):
    _ = get_translation(user_lang)

    return await client.send_message(
        callback.from_user.id, _("We are now uploading Tor Browser...")
    )


async def upload_succeeded(
        client: Client,
        callback: CallbackQuery,
        user_lang: str,
        platform: str,
):
    _ = get_translation(user_lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
             + _("Success!")
             + "</b>\n\n"
             + _("Here's your download")
             + f" <b>({platform}).</b> "
             + _("Stay safe!"),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + user_lang)]]
        ),
    )


async def upload_already_done(
        client: Client, callback: CallbackQuery, user_lang: str, platform: str
):
    callback_data = unpack_callback_data(callback)
    user_lang = callback_data[-1]
    _ = get_translation(user_lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
             + _("Success!")
             + "</b>\n\n"
             + _("Here's your Tor Browser")
             + f" <b>({platform})</b>. "
             + _("Stay safe!"),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + user_lang)]]
        ),
    )


async def upload_failed(
        client: Client,
        callback: CallbackQuery,
        lang: str,
        exception: Optional[Exception] = None,
        platform: Optional[str] = None,
):
    _ = get_translation(lang)
    support_username = "@TorProjectSupportBot"

    """
    By default, we log a very limited amount of information that is normally enough to
    track down the root cause of an error. However, by providing a detailed,
    transparent error message and a point of contact, we allow the user to provide us
    with more context, establish whether an error may be occuring due to normal usage
    and more. This is likely the easiest way to do this in our situation.
    """

    exception_str = str(exception)
    exception_text = ""
    if Exception is not None and len(exception_str) > 0:
        exception_text = "\n\n" + _("Reason: ") + f"`{exception_str}`"

    download_info = ""
    if platform is not None:
        download_info = f"(<b>{platform}</b>)"

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>"
             + _("Failure!")
             + "</b>\n\n"
             + _("Something went wrong during the upload! " "Please try again later. ")
             + exception_text
             + "\n\n☎️: {username} ".format(username=support_username)
             + download_info,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton(_("Main Menu"), "welcome:" + lang)]]
        ),
    )
