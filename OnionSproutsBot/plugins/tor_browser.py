# SPDX-FileCopyrightText: 2020-2024 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2022 Evangelos "GeopJr" Paterakis
# SPDX-FileCopyrightText: 2022-2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging
from datetime import timedelta
from typing import Tuple, Optional

import requests_cache
from pyrogram import Client, filters
from pyrogram.errors import QueryIdInvalid
from pyrogram.types import CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton

from OnionSproutsBot import helpers, files, ui, i18n
from OnionSproutsBot.config import data
from OnionSproutsBot.database import Database
from OnionSproutsBot.helpers import unpack_callback_data

session = requests_cache.CachedSession(data['bot']['cache_name'], expire_after=timedelta(minutes=10))

platforms = {
    "android-aarch64": "Android 64-bit",
    "android-armv7": "Android 32-bit",
    "ios": "iOS",
    "linux-i686": "Linux 32-bit",
    "linux-x86_64": "Linux 64-bit",
    "macos": "macOS",
    "windows-i686": "Windows 32-bit",
    "windows-x86_64": "Windows 64-bit",
    "windows7-i686": "Windows7 32-bit",
    "windows7-x86_64": "Windows7 64-bit",
}


def latest_onion_browser_urls() -> Tuple[str, str, None, None]:
    latest_release = session.get("https://api.github.com/repos/OnionBrowser/OnionBrowser/releases/latest").json()
    for asset in latest_release["assets"]:
        if asset["name"] == "OnionBrowser.ipa":
            return asset["browser_download_url"], f"OnionBrowser-{latest_release['name']}.ipa", None, None
    raise RuntimeError("The latest release of Onion Browser could not be found.")


def latest_windows7_browser_urls(platform: str) -> Tuple[str, str, str, str]:
    url_base = "https://dist.torproject.org/torbrowser"
    # let's find the latest version the hacky way
    version = None
    for i in range(9, 50):
        if session.get(f"{url_base}/13.5.{i}").status_code == 200:
            version = f"13.5.{i}"
    if version == None:
        raise RuntimeError("The latest release for windows7 could not be found.")

    arch = platform.removeprefix("windows7-")
    file_name = f"tor-browser-windows-{arch}-portable-{version}.exe"
    url = f"{url_base}/{version}/{file_name}"

    return url, file_name, f"{url}.asc", f"{file_name}.asc"


def latest_download_urls(platform: str) -> Tuple[str, str, Optional[str], Optional[str]]:
    if platform == "ios":
        return latest_onion_browser_urls()
    if platform.startswith("windows7-"):
        return latest_windows7_browser_urls(platform)
    
    download = session.get(f"{data['tor']['endpoint']}/download-{platform}.json").json()
    bin_url, sig_url = download["binary"], download["sig"]
    bin_name, sig_name = bin_url.rsplit("/", 1)[-1], sig_url.rsplit("/")[-1]
    return bin_url, bin_name, sig_url, sig_name


# Example query: request_tor:en
@Client.on_callback_query(filters.regex("^request_tor:[^:]+$"))
async def request_tor(client: Client, callback: CallbackQuery):
    callback_data = unpack_callback_data(callback)
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    platform_keyboard = []

    # Generate list of available platforms, then put
    # each of them in separate buttons.
    for platform_code, platform_name in platforms.items():
        platform_keyboard.append(
            InlineKeyboardButton(
                text=platform_name,
                # Here, "select_locale" may be a bit confusing,
                # but here, the buttons tell the bot to proceed
                # with select_locale, while telling it the platform that
                # was chosen.
                callback_data="download_tor:" + platform_code + ":" + lang,
            )
        )

    # After we collect all of the buttons that we should we have,
    # we create our keyboard, and then send it to the user.
    button_rows = ui.get_rows(platform_keyboard, 2)
    platform_markup = InlineKeyboardMarkup(button_rows)

    await client.send_message(
        callback.from_user.id,
        text="<b>"
             + _("Download Tor from Telegram")
             + "</b>\n\n"
             + _("Which operating system are you using?"),
        reply_markup=platform_markup,
    )

    await callback.answer()


# Example query: download_tor:linux64:it:en
@Client.on_callback_query(filters.regex("^download_tor:[^:]+:[^:]+$"))
async def download_tor(client: Client, callback: CallbackQuery):
    callback_data = unpack_callback_data(callback)
    lang = callback_data[-1]
    platform = callback_data[1]
    _ = i18n.get_translation(lang)

    logging.info(f"Binary ({platform}) requested.")
    logging.debug(f"Request: {platform} ({lang}) - Callback: {callback_data}")

    # Checks if the requested platform and locale exist. If not, the
    # whole process is aborted for security reasons.
    if platform not in platforms.keys():
        logging.critical(f"Binary ({platform}) NOT in list. Aborting...")
        return await callback.answer()

    # Fetch the download link for the requested files with the response.
    # We do this regardless of whether we already have cached the files
    # or not, as we cannot easily predict the file name and submit a
    # request anyways.
    bin_url, bin_name, sig_url, sig_name = latest_download_urls(platform)

    logging.debug(f"Requested signature URL: {sig_url}")
    logging.debug(f"Requested binary URL: {bin_url}")
    logging.debug(f"Requested signature name: {sig_name}")
    logging.debug(f"Requested binary name: {bin_name}")

    # We check if the file has already been uploaded to Telegram. If so,
    # there should be an entry in our database for it.
    results = await Database().search_file_in_db(bin_name)

    if results is not None:
        logging.info(f"Binary ({platform}) found!")
        logging.debug(f"Found binary cache ID: {results[1]}")
        logging.debug(f"Found signature cache ID: {results[3]}")

        # 1: File, 3: Signature
        await client.send_cached_media(callback.from_user.id, file_id=results[1])
        if sig_url is not None:
            await client.send_cached_media(callback.from_user.id, file_id=results[3])
        await helpers.upload_already_done(client, callback, lang, platform)
    else:
        try:
            logging.info(f"Binary ({platform}) not found. Uploading...")

            await client.send_message(
                callback.from_user.id,
                _(
                    "The version you have requested **(%s)** "
                    "hasn't been uploaded to Telegram's servers yet. "
                    "Please wait..."
                )
                % (platform),
            )

            if sig_url is None or sig_name is None:
                sig_name = f"{bin_name}.fake"
                sig_id = "DUMMY"
            else:
                reply = await helpers.sig_send_notification(client, callback, lang)
                sig_id = await files.relay_files(
                    client,
                    callback,
                    sig_url,
                    sig_name,
                    data["bot"]["download_path"],
                    reply,
                    lang,
                )

            reply = await helpers.file_send_notification(client, callback, lang)
            bin_id = await files.relay_files(
                client,
                callback,
                bin_url,
                bin_name,
                data["bot"]["download_path"],
                reply,
                lang,
            )

            logging.info("--- NEW ENTRY ---")
            logging.info(f"Binary name: {bin_name}")
            logging.info(f"Binary cache ID: {bin_id}")
            logging.info(f"Signature name: {sig_name}")
            logging.info(f"Signature cache ID: {sig_id}")

            await Database().insert_new_release(
                bin_name,
                bin_id,
                sig_name,
                sig_id,
            )

            logging.info("Database entry: Successful")
            logging.info("-----------------")
            await helpers.upload_succeeded(client, callback, lang, platform)

        except Exception as e:
            # Explicitly send a message to notify them of the failure.
            logging.exception(e)
            logging.info("Database entry: Likely failed")
            logging.info("-----------------")
            await helpers.upload_failed(client, callback, lang, e, platform)
    try:
        await callback.answer()
    except QueryIdInvalid as e:
        logging.warning(f"Error answering callback: {e}")
