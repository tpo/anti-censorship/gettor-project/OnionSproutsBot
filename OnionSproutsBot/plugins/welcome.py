# SPDX-FileCopyrightText: 2022-2024 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2022 Evangelos "GeopJr" Paterakis
# SPDX-FileCopyrightText: 2022-2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from typing import Union

from pyrogram import Client, filters
from pyrogram.errors import WebpageCurlFailed
from pyrogram.types import (CallbackQuery, InlineKeyboardButton,
                            InlineKeyboardMarkup, Message)

from OnionSproutsBot import i18n, ui
from OnionSproutsBot.config import data
from OnionSproutsBot.helpers import unpack_callback_data


@Client.on_message(filters.command("start"))
async def start_command(client: Client, message: Union[Message, CallbackQuery]):
    user = message.from_user
    # User is not provided if the message is in a channel.
    if not user:
        return None

    # Check if the language returned by Telegram is available.
    # If it is, send the welcome message, else send the language menu.
    user_lang = user.language_code
    if user_lang is None:
        user_lang = "en"
    else:
        user_lang = user_lang.lower()

    available_locales = i18n.available_locales.keys()
    if user_lang != "en" and user_lang in available_locales:
        return await send_welcome_message(client, message.from_user.id, user_lang)

    if isinstance(message, CallbackQuery):
        message = message.message

    await send_language_menu(client, message.chat.id, user_lang)


# Sends the language menu
async def send_language_menu(client: Client, chat_id: int, user_lang: str):
    # Set the translation to user_lang.
    _ = i18n.get_translation(user_lang)

    lang_rows = []
    available_locales = i18n.available_locales.keys()
    # The sort function is being used to reorder the list so user.language_code
    # is the first button, in a way to make it easier for the user to pick
    # their language if Telegram returned the correct one.
    for lang in sorted(available_locales, key=user_lang.__eq__, reverse=True):
        lang_rows.append(
            InlineKeyboardButton(
                text=f'{i18n.available_locales[lang]["full_name"]} ({lang})',
                callback_data="welcome:" + lang,
            )
        )

    button_rows = ui.get_rows(lang_rows, 3)

    lang_markup = InlineKeyboardMarkup(button_rows)

    try:
        await client.send_photo(
            chat_id=chat_id,
            photo=data["bot"]["language_photo"],
            caption="<b>"
            + _("Language selection")
            + "</b>\n\n"
            + _("Please select your language:"),
            reply_markup=lang_markup,
        )
    except (WebpageCurlFailed, KeyError):
        await client.send_message(
            chat_id=chat_id,
            text="<b>"
            + _("Language selection")
            + "</b>\n\n"
            + _("Please select your language:"),
            reply_markup=lang_markup,
        )


# Example query: change_lang:en
@Client.on_callback_query(filters.regex("^change_lang:[^:]+$"))
async def change_lang(client: Client, callback: CallbackQuery):
    # All callbacks need to get the language from now on.
    callback_data = unpack_callback_data(callback)
    lang = callback_data[-1]

    await send_language_menu(client, callback.from_user.id, lang)
    await callback.answer()


# Example query: welcome:en
@Client.on_callback_query(filters.regex("^welcome:[^:]+$"))
async def welcome_command(client: Client, callback: CallbackQuery):
    # All callbacks need to get the language from now on.
    callback_data = unpack_callback_data(callback)
    lang = callback_data[-1]

    await send_welcome_message(client, callback.from_user.id, lang)
    await callback.answer()


# Sends the welcome message
async def send_welcome_message(client: Client, user_id: int, lang: str):
    _ = i18n.get_translation(lang)
    bot_name = _("GetTor")

    # If lang is English, label = 'Change Language 🌐'
    # else label = "<'Change Language' translated> (Change Language) 🌐"
    if lang.startswith("en_") or lang == "en":
        change_lang_button_label = "Change Language 🌐"
    else:
        change_lang_button_label = _("Change Language") + " (Change Language) 🌐"

    await client.send_message(
        chat_id=user_id,
        text="<b>"
        + _("Welcome!")
        + "</b>\n\n"
        + _("Hi, welcome to {name}! ").format(name=bot_name)
        + _("What do you want me to do?"),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        _("Send me Tor Browser"), "request_tor:" + lang
                    )
                ],
                [InlineKeyboardButton(change_lang_button_label, "change_lang:" + lang)],
                [InlineKeyboardButton(_("More..."), "send_faq:" + lang)],
            ]
        ),
    )


@Client.on_callback_query(filters.regex("^send_faq:[^:]+$"))
async def send_faq(client: Client, callback: CallbackQuery):
    callback_data = unpack_callback_data(callback)
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<b>" + _("More...") + "</b>\n\n" + _("What do you want to know?"),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        _("Send me other mirrors for Tor Browser"),
                        "request_tor_mirrors:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("Send me Tor bridges."),
                        "request_tor_bridges:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("Explain what Tor bridges are."),
                        "explain_bridges:" + lang,
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("Explain what Tor is."),
                        "explain_tor:" + lang
                    )
                ],
                [
                    InlineKeyboardButton(
                        _("I have other questions."),
                        "request_support:" + lang
                    )
                ],
            ]
        ),
    )
