# SPDX-FileCopyrightText: 2022-2023 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2022 Justin "Justasic" Crawford
# SPDX-FileCopyrightText: 2022-2023 The Tor Project, Inc.
# SPDX-FileContributor: 2022-2023 irl <irl@sr2.uk>
#
# SPDX-License-Identifier: BSD-3-Clause

import logging
import os
from typing import Optional

import aiosqlite


class Database:
    _instance: Optional["Database"] = None
    conn: Optional[aiosqlite.Connection] = None

    def __new__(cls) -> "Database":
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls)
        return cls._instance

    async def connect(self, db_name: str, db_path: Optional[str]) -> None:
        db_path = os.path.join(db_path or os.getcwd(), db_name)
        logging.info(f"Database path: {db_path}")
        logging.info("Connecting to database!")
        try:
            self.conn = await aiosqlite.connect(db_path)
        except Exception as exc:
            logging.fatal(exc)
        logging.info("Connected!")

    async def cursor(self) -> aiosqlite.Cursor:
        if self.conn is None:
            raise RuntimeError("Tried to fetch a database cursor before connecting.")
        return await self.conn.cursor()

    async def close(self) -> None:
        if self.conn is None:
            raise RuntimeError("Tried to close database connection before connecting.")
        await self.conn.close()

    async def commit(self) -> None:
        if self.conn is None:
            raise RuntimeError("Tried to commit database transaction before connecting.")
        await self.conn.commit()

    async def search_file_in_db(self, filename: str):
        return await (
            await (await self.cursor()).execute(
                """SELECT binary, binary_id, sig, sig_id FROM tor_releases WHERE (
                 binary = ?
            );""",
                (filename,),
            )
        ).fetchone()

    async def create_empty_db(self) -> None:
        await (await self.cursor()).execute(
            """CREATE TABLE IF NOT EXISTS tor_releases (
                binary TEXT,
                binary_id TEXT,
                sig TEXT,
                sig_id TEXT,
                UNIQUE(binary, binary_id, sig, sig_id)
            );"""
        )
        await Database().commit()

    async def insert_new_release(
            self,
            bin_name: str,
            bin_id: str,
            sig_name: str,
            sig_id: str,
    ):
        await (await self.cursor()).execute(
            "INSERT INTO tor_releases VALUES (?, ?, ?, ?);",
            (bin_name, bin_id, sig_name, sig_id),
        )
        await Database().commit()
