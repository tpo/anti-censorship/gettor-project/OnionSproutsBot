# SPDX-FileCopyrightText: 2022-2023 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
# SPDX-FileContributor: 2023 irl <irl@sr2.uk>
#
# SPDX-License-Identifier: BSD-3-Clause

import OnionSproutsBot.bot

__version__ = "1.3.0"

if __name__ == "__main__":
    OnionSproutsBot.bot.init()
