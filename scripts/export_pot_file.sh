#!/bin/bash

# SPDX-FileCopyrightText: 2022 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

if [ -d "OnionSproutsBot" ]; then
    echo "Already possibly in top directory, proceeding..."
else
    echo "Not in directory, exiting..."
    exit 1
fi

echo "Exporting strings from Python source files..."

xgettext -L Python \
  --files-from=OnionSproutsBot/locales/POTFILES \
  --output=OnionSproutsBot/locales/onionsproutsbot.pot \
  --copyright-holder="The OnionSproutsBot Authors." \
  --package-name=OnionSproutsBot \
  --msgid-bugs-address="https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/OnionSproutsBot/issues" \
  --no-location

echo "Done."
exit 0
