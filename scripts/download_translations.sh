#!/bin/bash

# SPDX-FileCopyrightText: 2022 Panagiotis "Ivory" Vasilopoulos (n0toose)
# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

CWD="."

if [ -d "OnionSproutsBot" ]; then
    echo "Already possibly in top directory, proceeding..."
else
    echo "Not in directory, exiting..."
    exit 1
fi

echo "Checking if repository already exists..."

if [ -d "$CWD/../translation" ]; then
  echo "Folder exists. Trying to update repository..."
  git -C $CWD/../translation pull
else
  echo "Folder does not exist. Cloning..."
  # TODO: Eventually replace with completed translations only.
  git -C $CWD/.. \
    clone git@gitlab.torproject.org:tpo/translation.git \
    --branch=onionsproutsbot
fi

echo "Copying and pasting localization files..."
cp $CWD/../translation/onionsproutsbot+*.po $CWD/OnionSproutsBot/locales/
# TODO: Check if https://github.com/python-babel/babel/issues/993 is resolved and remove the following line if it is
rm -rf $CWD/locales/ay $CWD/OnionSproutsBot/locales/onionsproutsbot+ay.po # Not supported by python-babel

echo "Done."
exit 0
