<!--
SPDX-FileCopyrightText: 2023 Panagiotis "Ivory" Vasilopoulos (n0toose)

SPDX-License-Identifier: BSD-3-Clause
-->


# Contributing to OnionSproutsBot

Hi, thank you for taking the time to contribute to this project!

All types of contributions are encouraged and immensely appreciated. See the
[Table of Contents](#table-of-contents) for different ways to help and details
about how this project handles them. Please make sure to read the relevant
section before making your contribution. It will make it a lot easier for us
maintainers and smooth out the experience for all involved. We look forward
to cooperating with you! :)

## Table of Contents

- [Translations](#translations)
- [Reporting Bugs](#reporting-bugs)
  - [Users](#users)
  - [Instance Administrators](#instance-administrators)
- [Submitting Merge Requests](#submitting-merge-requests)
  - [Internationalization](#internationalization)
  - [Commit Messages](#commit-messages)
  - [Formatting](#formatting)

## Translations

You can contribute translations to this project on Weblate: https://hosted.weblate.org/projects/tor/

## Reporting Bugs

If you're experiencing a problem with this project, the right place to report
such issues is our [bug tracker](https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/OnionSproutsBot/-/issues).

For us to be able to help, we need to know as much about your problem as
possible.

Your report should contain the following information:

### Users

> This format should be preferred if you are reporting an issue on behalf of
> somebody else.

- What were you trying to do?
- What did you expect to happen? What happened instead?
- Did you get an error message? If yes, what did it say?

### Instance Administrators

- Operating System (Name / Version)
- Version (Release / Commit Revision)
- Config file *without* credentials

## Submitting Merge Requests

### Internationalization

> You can skip this section if your proposed change does not alter any strings
> that are visible to the user.

We use gettext to translate the bot between English and many other languages.
As this is relatively unusual for this kind of project, we find it important
to mention the steps to update the locale files, so as to keep this project
accessible to everyone.

1. Ensure that gettext is installed. (Debian: `sudo apt install gettext`)
2. Copy the file header (containing e.g. the project title) present in
   `OnionSproutsBot/locales/onionsproutsbot.pot`
3. From the top directory, run the following command:

```sh
xgettext -L Python \
	--files-from=OnionSproutsBot/locales/POTFILES \
	--output=OnionSproutsBot/locales/onionsproutsbot.pot \
	--copyright-holder="The OnionSproutsBot Authors." \
	--package-name=OnionSproutsBot \
	--msgid-bugs-address="https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/OnionSproutsBot/issues" \
	--no-location
```

4. Recover the old file header.

### Commit Messages

The summary of your commit should not contain more than *50 characters*. All
other lines should not contain more than [*70-75 characters*.](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/submitting-patches.rst?id=8e41e0a575664d26bb87e012c39435c4c3914ed9#n628).

Please try to be concise and precise about what your commit does. If you
believe that the Summary adequately explains it, then that's more than enough.

Please try to avoid mentioning any crucial information about your change in
the description of a Merge Request, and use the Commit for this purpose
instead, as its contents will be automatically used for that description
later.

An exception to this rule would be e.g. using the description of a
Merge Request to directly address another contributor over something that is
related to the review process, which probably isn't something that should
forever "engrave" the commit tree.

### Formatting

We use *flake8* for formatting. The maximum length of a line, as defined in
`setup.cfg`, is 132 columns.
